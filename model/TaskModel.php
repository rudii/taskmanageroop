<?php

include '../model/Database.php';

class TaskModel extends Db {
    
    public function getTasks() {
        $sql = "SELECT * FROM task;";
        $stmt = $this->connect()->query($sql);

        while ($row = $stmt->fetch()) {
            echo "<a href='/taskmanager/view/task_edit.php?id=" . $row["id"] . "'>" . $row['name'] . "</a>-" . $row['status']. "<br>";
        }
    }
    
    public function getTask($id) {
        $sql = "SELECT * FROM task WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);

        $results = $stmt->fetchAll();
        return $results[0];
    }

    public function setTask($name, $status="1") {
        $sql = "INSERT INTO task (name, status) VALUES (?, ?);";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$name, $status]);

        return;
    }

    public function updateTask($id, $name, $status="1") {
        $sql = "UPDATE task SET name = ?, status = ? WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$name, $status, $id]);

        return;
    }

    public function deleteTask($id) {
        $sql = "DELETE FROM task WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);

        return;
    }
    
}