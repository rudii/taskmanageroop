<?php

include '../model/Database.php';

class UserModel extends Db {

    private $id;
    private $name;
    private $password;

     function __construct($id, $username, $password)
     {
        $this->id = $id;
        $this->name = $username;
        $this->password = $password;
     }
    
    protected function getUsers() {
        $sql = "SELECT username FROM user;";
        $stmt = $this->connect()->query($sql);

        while ($row = $stmt->fetch()) {
            echo "<a href='/usermanager/view/user_edit.php?id=" . $row["id"] . "'>" . $row['name'] . "</a><br>";
        }
    }
    
    protected function getUser() {
        $sql = "SELECT username FROM user WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$this->id]);

        $results = $stmt->fetchAll();
        return $results[0];
    }

    protected function setUser() {
        $sql = "INSERT INTO user (username, password) VALUES (?, ?);";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$this->name, $this->password]);

        return;
    }

    protected function updateUser() {
        $sql = "UPDATE user SET username = ?, password = ? WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$this->name, $this->password, $this->id]);

        return;
    }

    public function deleteUser() {
        $sql = "DELETE FROM user WHERE id = ?;";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$this->id]);

        return;
    }
    
}