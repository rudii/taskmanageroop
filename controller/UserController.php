<?php

include '../model/UserModel.php';

class UserController extends UserModel {
    private $id;
    private $username;
    private $password;

    public function __construct() {
        $this->id = $_POST['id'];
        $this->username = $_POST['username'];
        $this->password = $_POST['password'];
    }

    public function createUser() {
        $this->setUser($this->username, $this->password);
        header("Location: /taskmanager/view/user_list.php?user=created");
    }

    public function saveUser() {
        $this->updateUser($this->id, $this->username, $this->password);
        header("Location: /taskmanager/view/user_list.php?user=updated");
    }

    public function removeUser() {
        $this->deleteUser($this->id);
        header("Location: /taskmanager/view/user_list.php?user=deleted");
    }

    public function loginUser() {
        $this->getUser($this->username, $this->password);
        header("Location: /taskmanager/view/user_list.php?user=logged");
    }

}