<?php

include '../model/TaskModel.php';

class TaskController extends TaskModel {
    private $id;
    private $name;
    private $status;

    public function __construct() {
        $this->id = $_POST['id'];
        $this->name = $_POST['name'];
        $this->status = $_POST['status'];
    }

    public function createTask() {
        $this->setTask($this->name, $this->status);
        header("Location: /taskmanager/view/task_list.php?task=created");
    }

    public function saveTask() {
        $this->updateTask($this->id, $this->name, $this->status);
        header("Location: /taskmanager/view/task_list.php?task=updated");
    }

    public function removeTask() {
        $this->deleteTask($this->id);
        header("Location: /taskmanager/view/task_list.php?task=deleted");
    }

}