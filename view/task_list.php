<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tasks</title>
</head>
<body>
    <h4>New Task</h4>
    <form action="/taskmanager/view/task_update.php" method="POST">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="status" placeholder="Status">
        <button type="submit" name="submit" value="create">Send</button>
    </form>

    <br><br>
    <h4>Existing Tasks</h4>
    <?php
        include '../model/TaskModel.php';

        $product = new TaskModel();
        $product->getTasks();
    
    ?>
</body>
</html>