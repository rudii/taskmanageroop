<?php
    include '../model/TaskModel.php';

    $taskV = new TaskModel();
    $task = $taskV->getTask($_GET['id']);

    $id = $task['id'];
    $name = $task['name'];
    $status = $task['status']
?>

<h4>Edit Task</h4>
<form action="/taskmanager/view/task_update.php" method="POST">
    <input type="text" name="name" placeholder="Name" value="<?php echo $name?>">
    <input type="text" name="status" placeholder="Status" value="<?php echo $status?>">
    <input type="hidden" name="id" value="<?php echo $id?>">
    <button type="submit" name="submit" value="update">Send</button>
</form>
<br>
<br>
<br>
<br>
<br>

<!-- delete form -->
<form action="/taskmanager/view/task_update.php" method="POST">
    <input type="hidden" name="id" value="<?php echo $id?>">
    <button type="submit" name="submit" value="delete">Delete</button>