<?php

include '../controller/UserController.php';

$user = new UserController($_POST['id'], $_POST['name'], $_POST['password']);

if ($_POST['submit'] == 'register') {
    $user->createUser();
} else if ($_POST['submit'] == 'login') {
    $user->loginUser();
} else if ($_POST['submit'] == 'update') {
    $user->saveUser();
} else if ($_POST['submit'] == 'delete') {
    $user->removeUser();
} else {
    header("Location: /taskmanager/view/login.php");
}