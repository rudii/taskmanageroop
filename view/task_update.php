<?php

include '../controller/TaskController.php';

$task = new TaskController($_POST['id'], $_POST['name'], $_POST['status']);

if ($_POST['submit'] == 'create') {
    $task->createTask();
} else if ($_POST['submit'] == 'update') {
    $task->saveTask();
} else if ($_POST['submit'] == 'delete') {
    $task->removeTask();
} else {
    header("Location: /taskmanager/view/task_list.php");
}